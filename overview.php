<?php
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Tabcontent -->
    <title>MyHomework - Overview</title>

    <!-- PHP -->
    <?php
      include 'codeConstants.php';
      $constants = NEW Constants();
      $constants -> writeHead();
    ?>

  </head>
  <body onload='validateLastLocation("signInValidation.php")'>
    <?php
      $constants -> writeLoggedInHeader();
    ?>
    <main>
      <?php
          //check if the session variable is set
          if ($_SESSION["UserID"] != null) {
            $pdo = connectDatabase();

                // if (isset($_GET['orderBy'])) {
                //   //if the variable is set the table is orderd by it
                //   $orderBy = $_GET['orderBy'];
                  
                // }
                // else {
                //   //by default it's the completiondate
                //   $orderBy = "CompletionDate";
                // }

                $statement = $pdo->prepare("SELECT * FROM `entry` WHERE UserID = ? ORDER BY CompletionDate ASC");
                $statement->execute(array($_SESSION["UserID"])); 

                //Deletes the entry with ID if it's set
                if (isset($_GET['finishID'])) {
                  $id = $_GET['finishID'];
                  $userId = $_SESSION["UserID"];

                  $deletestatement = $pdo->prepare("DELETE FROM `entry` WHERE UserID = ? AND EntryID = ?");
                  $deletestatement->execute(array($_SESSION["UserID"], $id)); 

                  header('Location: overview.php');
                }
  
                //creates the table with all entries
                if ($statement->rowCount() > 0) {
                  echo '
                  <table class="highlight responsive-table">
                      <thead>
                        <tr>
                            <th width="20%">Subject</th>
                            <th width="50%">Description</th>
                            <th width="20%">Completion date</th>
                            <th width="5%">Edit</th>
                            <th width="5%">Finish</th>
                        </tr>
                      </thead>
                      <tbody>';

                  //writes each entry from the database into the table    
                  while($row = $statement->fetch()) {
                    if (strip_tags($row["Type"])== "Exam") {
                      //If the type is Exam the background color changes to red
                      echo '<tr style="background-color:red">';
                    }
                    elseif (strip_tags($row["CompletionDate"]) < date("Y-m-d")) {
                      //If the completiondate is in the past the backgroundcolor changes to gray
                      echo '<tr style="background-color:LightGray">';
                    }
                    else {
                      echo '<tr>';
                    }

                    //print the values from the entry
                    echo '<td>' .strip_tags($row["Subject"]). '</td>';
                    echo '<td>' .strip_tags($row["Description"]). '</td>';
                    echo '<td>' .strip_tags($row["CompletionDate"]). '</td>';
                    echo '<td><a href="entry.php?editID=' .strip_tags($row["EntryID"]). '" style="color:black" >&nbsp;&nbsp;<i class="fas fa-pen"></i></td>';
                    echo '<td><a href="overview.php?finishID=' .strip_tags($row["EntryID"]). '" style="color:black" >&nbsp;&nbsp;<i class="fas fa-check"></i></td></tr>';
                  }
                  echo"</tbody></table>";
                }
                else {
                  //No entries for this user.
                  echo "<h1>No homework left to do :)</h1>";
                }
            }    
          
          else {
            //The Sesseionvariable is not set
            header("Location: index.php");
          }
      ?>
      <!-- Space after table -->
      <div class="row"></div>
      <div class="row">
        <div id="add" class="col s10 m11"></div>
          <!-- Add button -->
          <a class="btn-floating btn-large waves-effect waves-light red" href="entry.php"><i class="fas fa-plus"></i></a>
      </div>
    </main>
  </body>
</html>
