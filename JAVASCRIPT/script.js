function validateLastLocation(searchterm) {
  //get the location where the user came from
  var location = document.referrer;

  //if the location contains the searchterm the False-Toast will appear
  if (location.search(searchterm) != -1) {
    showToast(false);
  }
}

function showToast(successful) {
  //shows a toast with a text
  if (!successful) {
    //False-Toast
    M.toast({ html: "Invalid Input", classes: "red darken-1" });
  }
}

function confirmBtnClick() {
  //The user has to confirm his click
  if (confirm('Are you sure you want to sign out?')) {
    //Log out
    window.location = "logout.php";
  } else {
    // Do nothing!
  }
}

